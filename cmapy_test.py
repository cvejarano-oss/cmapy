"""
Basic tests, mostly to make sure that nothing is broken in the CI pipeline.
"""

import cmapy
import numpy as np


def test_version_exists():
    """Check that no error is raised when using __version__"""
    print(cmapy.__version__)


def test_color_viridis():
    assert cmapy.color("viridis", 0) == [84, 1, 68]
    assert cmapy.color("viridis", 255) == [36, 231, 253]


def test_colorize():
    cmapy.colorize(np.zeros([10, 10, 3], dtype=np.uint8), "viridis")
