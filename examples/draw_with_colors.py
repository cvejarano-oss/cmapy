#!/usr/bin/env python
"""Get individual colors from colormaps to draw.
   This is not the optimal way to draw a linear gradient,
   it is meant only to show how to use cmapy to choose colors."""

import numpy as np
import cv2
import cmapy

# Create an empty image.
im_size = 200
img = np.zeros((im_size, im_size, 3), np.uint8)

# Draw a vertical gradient using the 'hot' colormap.
for i in range(im_size):
    color = cmapy.color("hot", float(i) / im_size)
    img[i, :, :] = color

# Display
cv2.imshow("Image", img)
cv2.waitKey(0)
